# Raspberry Modular Sequencer
## Instalação e Execução do Kivy
**Debian**
Acesse a pasta do projeto via terminal e execute <code>make install-dependencies</code> para instalar as dependencias.
Já na pasta do projeto execute <code>make run</code> .

**Demais Distros**
Com seu package manager favorito, instale a versão mais recente do kivy, assim como python3, com versão de perferência superior a 3.6
No diretório do projeto, execute <code>python3 app.py</code>.

## Contribuindo
Para contribuir no projeto, atente-se aos seguintes pontos antes de um merge:

* Se o código está devidamente padronizado com [black](https://github.com/psf/black)
* Se os imports estão sortidos corretamente com o [isort](https://pypi.org/project/isort/)
* Se o linter não retorna erro

Para execução automatizada, **no diretório do projeto**, torne a pasta scripts executável com <code>sudo chmod +x -R ./scripts</code> e, depois, execute <code>./scripts/text_refactor.sh</code> . O script irá executar primeiro o black, depois o isort e depois o linter. Atente-se para as reclamações do terminal, principalmente as sugestões do isort. 
